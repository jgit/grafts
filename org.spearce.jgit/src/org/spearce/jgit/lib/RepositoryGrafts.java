package org.spearce.jgit.lib;

import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class RepositoryGrafts {

    private Map<AnyObjectId, List<AnyObjectId>> parentMap;
    private File file;

    public RepositoryGrafts(File file) {
        this.file = file;
        parentMap = new HashMap<AnyObjectId, List<AnyObjectId>>();
        try {
            load();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void load() throws IOException {
        parentMap.clear();
        if (file.exists()) {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line;
            while ((line = reader.readLine()) != null) {
                if (!line.startsWith("#")) {
                    String[] split = line.split(" ");
                    if(split.length>=2){
                        ObjectId child = ObjectId.fromString(split[0]);
                        List<AnyObjectId> parents = new ArrayList<AnyObjectId>(split.length - 1);
                        for (int i = 1; i < split.length; i++) {
                            parents.add(ObjectId.fromString(split[i]));
                        }

                        parentMap.put(child, parents);
                    }
                }
            }
            reader.close();
        }
    }

    public List<AnyObjectId> getParents(AnyObjectId id) {
        List<AnyObjectId> list = parentMap.get(id.toObjectId().copy());
        if (list == null)
            return Collections.emptyList();
        else
            return list;
    }

    public File getFile() {
        return file;
    }
}
